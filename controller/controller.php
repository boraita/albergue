<?php 

if ($postdata = file_get_contents("php://input")) {
    $request = json_decode($postdata);
    if (file_exists("idiomas/" . strtolower($request->idioma) . ".txt"))
            echo json_encode(parse_ini_file("idiomas/" . strtolower($request->idioma) . ".txt"));
}
 ?>