var app = angular.module('moduloPrincipal',[]);
app.directive('myMap', function() {
    // directive link function
    var link = function(scope, element, attrs) {
        var map, infoWindow;
        var markers = [];
        
        // map config
        var mapOptions = {
            center: new google.maps.LatLng(38.389866, -6.412886),
            zoom: 16,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            scrollwheel: false
        };
        // map config
        var mapOptions2 = {
        		center: new google.maps.LatLng(38.389757, -6.413010),
        		zoom: 16,
        		mapTypeId: google.maps.MapTypeId.ROADMAP,
        		scrollwheel: false
        };
        
        // init the map
        function initMap() {
            if (map === void 0) {
                map = new google.maps.Map(element[0], mapOptions);
            }
        }    
        
        // place a marker
        function setMarker(map, position, title, content) {
            var marker;
            var markerOptions = {
                position: position,
                map: map,
                title: title,
                icon: '../src/img/albergue.png'
            };
            

            marker = new google.maps.Marker(markerOptions);
            markers.push(marker); // add marker to array
            
            google.maps.event.addListener(marker, 'click', function () {
                // close window if not undefined
                if (infoWindow !== void 0) {
                    infoWindow.close();
                }
                // create new window
                var infoWindowOptions = {
                    content: content
                };
                infoWindow = new google.maps.InfoWindow(infoWindowOptions);
                infoWindow.open(map, marker);
            });
        };
        
     // place a marker
        function setMarker2(map, position, title, content) {
        	var marker;
        	var markerOptions2 = {
        			position: position,
        			map: map,
        			title: title,
        			icon: '../src/img/ermita.png'
        	};
        	 marker = new google.maps.Marker(markerOptions2);
             markers.push(marker); // add marker to array
             
             google.maps.event.addListener(marker, 'click', function () {
                 // close window if not undefined
                 if (infoWindow !== void 0) {
                     infoWindow.close();
                 }
                 // create new window
                 var infoWindowOptions = {
                     content: content
                 };
                 infoWindow = new google.maps.InfoWindow(infoWindowOptions);
                 infoWindow.open(map, marker);
             });
        };
        
        // show the map and place some markers
        initMap();
        
        setMarker(map, new google.maps.LatLng(38.389866, -6.412886), 'Albergue', 'Albergue la Ermita');
        setMarker2(map, new google.maps.LatLng(38.389757, -6.413010), 'Ermita', 'Monumeto la ermita');
    };
    
    return {
        restrict: 'A',
        template: '<div id="gmaps"></div>',
        replace: true,
        link: link
    };
});

app.controller('PrincipalPage', function($scope,$http){
	
	
	$scope.idiomaPrimero = 'Español';
	$scope.idiomaSegundo = 'English';
	$scope.idiomaTercero = 'Deutsch';
	
	$scope.cambioIdioma = function($idioma){
		$http.post("../controller/controller.php", {idioma: $idioma})
		.success(function(respuesta){
			$scope.textoIdioma = respuesta;
        });
	};
	
});